const knex = require("../dbConfig");

module.exports = {
    async show(req, res) {
        const response = new Array()
        const data = await knex("reports").select(
            "id",
            "description",
            "type",
            "publication_id",
            "created_at",
            "updated_at"
        )
        if (!data[0]) {
            res.status(200).json({ erro: false, msg: 'Sem Denuncias!' });
        } else {

            const dataPublics = [];
            for (let i = 0; i < data.length; i++) {
                if (data[i].type === 1) {
                    const suggestions = await knex("suggestion")
                        .select(
                            "s.id as id_suggestion",
                            "s.suggestion",
                            "s.created_at",
                            "s.user_id",
                            "u.name",
                            "u.image",
                        )
                        .from("suggestion as s")
                        .leftJoin("user as u", "s.user_id", "u.id")
                        .where({ 's.id': data[i].publication_id })
                    if (suggestions != null) {
                        dataPublics.push({
                            id_suggestion: suggestions[0].id_suggestion,
                            suggestion: suggestions[0].suggestion,
                            created_at: suggestions[0].created_at,
                            user_id: suggestions[0].user_id,
                            image: suggestions[0].image,
                            name: suggestions[0].name,
                            type: data[i].type,
                            idReport: data[i].id,
                            reportDesc: data[i].description,
                            reportIdPublication: data[i].publication_id
                        });
                    }
                    response.push(...dataPublics)
                }
                else if (data[i].type === 2) {
                    const dataPublics = [];
                    const dataComment = await knex("comment")
                        .select(
                            "id",
                            "publication_id",
                        )
                        .where({ id: data[i].publication_id })

                    const publics = await knex("publication")
                        .select(
                            "p.id as idPublication",
                            "p.publication",
                            "p.created_at",
                            "u.name",
                            "u.image",
                            "j.position"
                        )
                        .from("publication as p")
                        .orderBy('idPublication', "desc")
                        .leftJoin("user as u", "p.user_id", "u.id")
                        .leftJoin("job_position as j", "u.job_position_id", "j.id")
                        .where({ 'p.id': dataComment[0].publication_id })

                    if (publics.length >= 1) {
                        for (let e = 0; e < publics.length; e++) {
                            const comment = await knex("comment")
                                .where({ 'c.publication_id': publics[e].idPublication })
                                .select(
                                    "c.id as idComment",
                                    "c.publication_id",
                                    "c.comment",
                                    "c.created_at",
                                    "u.name",
                                    "u.image",
                                    "u.job_position_id",
                                    "j.position"
                                )
                                .from("comment as c")
                                .leftJoin("user as u", "c.user_id", "u.id")
                                .leftJoin("job_position as j", "u.job_position_id", "j.id")

                            dataPublics.push({
                                idPublication: publics[e].idPublication,
                                publication: publics[e].publication,
                                created_at: publics[e].created_at,
                                name: publics[e].name,
                                image: publics[e].image,
                                position: publics[e].position,
                                type: data[i].type,
                                idReport: data[i].id,
                                reportDesc: data[i].description,
                                reportIdPublication: data[i].publication_id,
                                comment: comment,
                            });
                        }
                        response.push(...dataPublics);
                    }
                } else if (data[i].type === 3) {
                    const dataPublic = []
                    const publics = await knex("publication")
                        .select(
                            "p.id as idPublication",
                            "p.publication",
                            "p.created_at",
                            "u.name",
                            "u.image",
                            "j.position"
                        )
                        .from("publication as p")
                        .orderBy('idPublication', "desc")
                        .leftJoin("user as u", "p.user_id", "u.id")
                        .leftJoin("job_position as j", "u.job_position_id", "j.id")
                        .where({ 'p.id': data[i].publication_id })
                    if (publics.length >= 1) {
                        for (let e = 0; e < publics.length; e++) {
                            const comment = await knex("comment")
                                .where({ 'c.publication_id': publics[e].idPublication })
                                .select(
                                    "c.id as idComment",
                                    "c.publication_id",
                                    "c.comment",
                                    "c.created_at",
                                    "u.name",
                                    "u.image",
                                    "u.job_position_id",
                                    "j.position",
                                )
                                .from("comment as c")
                                .leftJoin("user as u", "c.user_id", "u.id")
                                .leftJoin("job_position as j", "u.job_position_id", "j.id")

                            if (!comment.length) {
                                dataPublic.push({
                                    idPublication: publics[e].idPublication,
                                    publication: publics[e].publication,
                                    created_at: publics[e].created_at,
                                    name: publics[e].name,
                                    image: publics[e].image,
                                    position: publics[e].position,
                                    type: data[i].type,
                                    idReport: data[i].id,
                                    reportDesc: data[i].description,
                                    reportIdPublication: data[i].publication_id
                                });
                            } else {
                                dataPublic.push({
                                    idPublication: publics[e].idPublication,
                                    publication: publics[e].publication,
                                    created_at: publics[e].created_at,
                                    name: publics[e].name,
                                    image: publics[e].image,
                                    position: publics[e].position,
                                    type: data[i].type,
                                    idReport: data[i].id,
                                    reportDesc: data[i].description,
                                    reportIdPublication: data[i].publication_id,
                                    comment: comment,
                                });
                            }
                            response.push(...dataPublic)
                        }
                    }
                }
            }
            res.status(200).json({ erro: false, data: response })
        }
    },
    async report(req, res) {
        const { id, description, type } = req.body;

        if (!id || !description || !type) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        try {
            const newReport = await knex("reports").insert({
                description: description,
                type: type,
                publication_id: id
            });
            res.status(200).json({ erro: false, msg: "Denuncia Cadastrada Com Sucesso!" });
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    },
    async destroy(req, res) {
        const { id } = req.params;
        if (!id) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        try {
            const reports = await knex("reports").where({ id: Number(id) });
            if (reports.length) {
                await knex("reports").del().where({ id });
                res.status(200).json({ erro: false, msg: "Denuncia Deletada!!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: "Denuncia Não Encontrada!!" });
        }
    },
    async destroypublics(req, res) {
        const { idReport, type, idPublication } = req.body;
        if (!idReport || !type || !idPublication) {
            res.status(400).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        if (type === 1) {
            try {
                const suggestion = await knex("suggestion").where({ id: Number(idPublication) });
                const reports = await knex("reports").where({ id: Number(idReport) });
                const reportsPublic = await knex("reports").where({ publication_id: Number(idPublication) });

                if (suggestion.length && reports.length) {
                    if (reportsPublic.length) {
                        for (let i = 0; i < reportsPublic.length; i++) {
                            await knex("reports").del().where({ id: Number(reportsPublic[i].id) });
                        }
                    }
                    await knex("reports").del().where({ id: Number(idReport) });
                    await knex("suggestion").del().where({ id: Number(idPublication) });
                    res.status(200).json({ erro: false, msg: "Ideia e Denuncia Deletada" });
                }
            } catch (error) {
                res.status(400).json({ erro: true, msg: "Solicitação Não Encontrada!" });
            }
        } else if (type === 2) {
            try {
                const comment = await knex("comment").where({ id: Number(idPublication) });
                const reports = await knex("reports").where({ id: Number(idReport) });
                const reportsPublic = await knex("reports").where({ publication_id: Number(idPublication) });

                if (comment.length && reports.length) {
                    if (reportsPublic.length) {
                        for (let i = 0; i < reportsPublic.length; i++) {
                            await knex("reports").del().where({ id: Number(reportsPublic[i].id) });
                        }
                    }
                    await knex("comment").del().where({ id: Number(idPublication) });
                    await knex("reports").del().where({ id: Number(idReport) });
                    res.status(200).json({ erro: false, msg: "Comentario e Denuncia Deletada" });
                }
            } catch (error) {
                res.status(400).json({ erro: true, msg: "Solicitação Não Encontrada!" });
            }
        } else if (type === 3) {
            try {
                const publication = await knex("publication").where({ id: Number(idPublication) });
                const reports = await knex("reports").where({ id: Number(idReport) });
                const reportsPublic = await knex("reports").where({ publication_id: Number(idPublication) });

                if (publication.length) {
                    const comment = await knex("comment").where({ publication_id: Number(publication[0].id) });
                    if (comment.length) {
                        for (let i = 0; i < comment.length; i++) {
                            await knex("comment").del().where({ publication_id: publication[0].id });
                        }
                    }
                }
                if (publication.length && reports.length) {
                    await knex("publication").del().where({ id: idPublication });
                    await knex("reports").del().where({ id: idReport });
                    if (reportsPublic.length) {
                        for (let i = 0; i < reportsPublic.length; i++) {
                            await knex("reports").del().where({ id: Number(reportsPublic[i].id) });
                        }
                    }
                }
                res.status(200).json({ erro: false, msg: "Publicação e Denuncia Deletada" });
            } catch (error) {
                res.status(400).json({ erro: true, msg: "Solicitação Não Encontrada!" });
            }
        } else {
            res.status(400).json({ erro: true, msg: "Solicitação Não Encontrada!" });
        }

    },
}
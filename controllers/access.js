const knex = require("../dbConfig");

module.exports = {

    async show(req, res) {
        const data = await knex("access_level").select(
            "id",
            "description",
            "level",
            "created_at",
            "updated_at"
        )
        res.status(200).json({ Level: data });
    },
    async store(req, res) {
        const { description, level } = req.body;
        if (!description || !level) {
            res.status(200).json({ erro: true, msg: "Enviar dados Corretamente!" });
            return;
        }
        try {
            const data = await knex("access_level").where({ level });
            if (data.length) {
                res.status(200).json({ erro: true, msg: "Acesso ja existe!" });
                return;
            }
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }

        try {
            const newAcces = await knex("access_level").insert({
               description: description,
               level: level
            });
            res.status(200).json({ erro: false, msg: "Acesso Cadastrado Com Sucesso!" });
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    },
}

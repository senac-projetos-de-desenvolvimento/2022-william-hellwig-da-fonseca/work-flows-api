const knex = require("../dbConfig");

module.exports = {
    async show(req, res) {
        const data = await knex("work_point")
            .select(
                "w.id as idPoint",
                "w.workpoint",
                "w.created_at",
                "u.name",
            )
            .from("work_point as w")
            .orderBy('w.created_at', "desc")
            .leftJoin("user as u", "w.user_id", "u.id");

        res.status(200).json({ erro: false, data: data });
    },
    async showLast(req, res) {
        const data = await knex("last_work_point")
            .select(
                "w.id as idPoint",
                "w.workpoint",
                "w.created_at",
                "u.name",
            )
            .from("last_work_point as w")
            .orderBy('w.created_at', "desc")
            .leftJoin("user as u", "w.user_id", "u.id");

        res.status(200).json({ erro: false, data: data });
    },
    async userPoint(req, res) {
        const { user_id, date } = req.body;
        if (!user_id && date) {
            const data = await knex("work_point")
                .where('w.created_at', '>=', `${date}T00:00:00.000Z`)
                .where('w.created_at', '<', `${date}T23:59:59.000Z`)
                .select(
                    "w.id as idPoint",
                    "w.workpoint",
                    "w.created_at",
                    "u.name",
                )
                .from("work_point as w")
                .orderBy('idPoint', "desc")
                .leftJoin("user as u", "w.user_id", "u.id")
                .limit(10);

            res.status(200).json({ erro: false, data: data });
        }
        if (user_id && !date) {
            const data = await knex("work_point").where({ user_id })
                .select(
                    "w.id as idPoint",
                    "w.workpoint",
                    "w.created_at",
                    "u.name",
                )
                .from("work_point as w")
                .orderBy('idPoint', "desc")
                .leftJoin("user as u", "w.user_id", "u.id")
                .limit(10);

            res.status(200).json({ erro: false, data: data });
        }
        if (user_id && date) {
            const data = await knex("work_point")
                .where({ user_id: user_id })
                .where('w.created_at', '>=', `${date}T00:00:00.000Z`)
                .where('w.created_at', '<', `${date}T23:59:59.000Z`)
                .select(
                    "w.id as idPoint",
                    "w.workpoint",
                    "w.created_at",
                    "u.name",
                )
                .from("work_point as w")
                .orderBy('idPoint', "desc")
                .leftJoin("user as u", "w.user_id", "u.id")
                .limit(10);

            res.status(200).json({ erro: false, data: data });
        }

    },
    async createPoint(req, res) {
        const { idUser, workpoint } = req.body;
        if (!idUser || workpoint == null) {
            res.status(200).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            const oldWorkPosition = await knex("last_work_point").select("id", "user_id").where({ user_id: idUser })

            if (oldWorkPosition.length) {
                await knex("last_work_point").update({ workpoint: workpoint }).where({ id: oldWorkPosition[0].id })
            } else {
                await knex("last_work_point").insert({ user_id: idUser, workpoint: workpoint })
            }
            await knex("work_point").insert({ user_id: idUser, workpoint: workpoint });
            res.status(200).json({ erro: false, msg: "Seu Ponto foi Salvo!" });
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    }
}
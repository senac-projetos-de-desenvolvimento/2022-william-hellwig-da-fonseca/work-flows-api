const multer = require('multer');
const parser = multer({ dest: 'public/uploads/' })
const knex = require("../dbConfig");


module.exports = {
    async imagePost(req, res) {
        parser.single('image')(req, res, err => {
            const id = Number(req.body.id);
            const image = {};
            image.id = req.file.filename;
            image.url = `/uploads/${image.id}`;

            save(image.url);

            async function save(url) {
                try {
                    const user = await knex("user").update("image", url).where({ id });
                    console.log(user)
                    res.status(200).json({ erro: false, msg: "Imagem atuaizada!!!", url: url });
                } catch (error) {
                    res.status(200).json({ erro: true, msg: error.message });
                }
            }
        })
    }
}
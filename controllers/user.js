const bcrypt = require("bcrypt");
const knex = require("../dbConfig");

module.exports = {
  async show(req, res) {
    const data = await knex("user").select(
      "u.id",
      "u.job_position_id",
      "j.position",
      "u.name",
      "u.location",
      "u.image",
      "u.email",
      "u.fone",
      "u.ctps",
      "u.score",
      "u.status",
      "u.created_at",
      "u.updated_at"
    )
      .from("user as u")
      .orderBy("j.position", 'ASC')
      .leftJoin("job_position as j", "u.job_position_id", "j.id");

    res.status(200).json({ Users: data });
  },

  async getScore(req, res) {
    const data = await knex("user").select(
      "id",
      "name",
      "image",
      "score",
    )
      .orderBy("score", 'DESC')

    res.status(200).json({ Users: data });
  },

  async updateScore(req, res) {
    const { id, score } = req.body;
    if (!id) {
      res.status(400).json({ erro: true, msg: "Envie os Dados Corretamente!." });
      return;
    }
    try {
      if (id === 2606) {
        await knex("user").update({ score: score });
        res.status(200).json({ erro: false, msg: "Pontos Atualizados!" });
      } else {
        await knex("user").update({ score: score }).where({ id: id })
        res.status(200).json({ erro: false, msg: "Pontos Atualizados!" });
      }
    } catch (error) {
      res.status(200).json({ erro: true, msg: error.message });
    }
  },

  async createUser(req, res) {
    const { name, email, fone, ctps, password, location, job_position_id } = req.body;
    if (!name || !email || !fone || !ctps || !password || !location || !job_position_id) {
      res.status(200).json({ erro: true, msg: "Enviar dados do colaborador!" });
      return;
    }
    try {
      const data = await knex("user").where({ ctps });
      if (data.length) {
        res.status(200).json({ erro: true, msg: "Colaborador Já Cadastrado!" });
        return;
      }
    } catch (error) {
      res.status(200).json({ erro: true, msg: error.message });
    }

    const hash = bcrypt.hashSync(password, 10);

    try {
      const newUser = await knex("user").insert({
        name,
        email,
        location,
        fone,
        ctps,
        password: hash,
        job_position_id: job_position_id
      });
      res.status(200).json({ erro: false, msg: "Colaborador Cadastrado Com Sucesso!" });
    } catch (error) {
      res.status(200).json({ erro: true, msg: error.message });
    }
  },

  async update(req, res) {
    const { id, password, newpassword } = req.body;
    if (!password || !newpassword) {
      res.status(400).json({ erro: true, msg: "Enviar Senha." });
      return;
    }
    try {

      const usuario = await knex("user").where({ id });
      if (bcrypt.compareSync(password, usuario[0].password)) {
        const hash = bcrypt.hashSync(newpassword, 10);
        await knex("user").update("password", hash).where({ id });
        res.status(200).json({ erro: false, msg: "Senha Alterada." });
      } else {
        res.status(200).json({ erro: true, msg: "Senha Incorreta" });
      }
    } catch (error) {
      res.status(200).json({ erro: true, msg: error.message });
    }
  },

  async upJobPosition(req, res) {
    const { id, name, email, fone, ctps, newPassword, location, job_position_id, status } = req.body;

    if (!id || !name || !email || !fone || !ctps || !location || !job_position_id || !status) {
      res.status(200).json({ erro: true, msg: "Enviar dados do colaborador!" });
      return;
    }
    if (newPassword === null) {
      try {
        const usuario = await knex("user").update({
          name: name,
          email: email,
          fone: fone,
          ctps: ctps,
          location: location,
          job_position_id: job_position_id,
          status: status
        }).where({ id: id });
        res.status(200).json({ erro: false, msg: "Cadastro Atualizado Com Sucesso!!!" });
      } catch (error) {
        res.status(200).json({ erro: true, msg: error.message });
      }
    } else {
      const hash = bcrypt.hashSync(newPassword, 10);
      try {
        const response = await knex("user").update({
          name: name,
          email: email,
          fone: fone,
          ctps: ctps,
          location: location,
          password: hash,
          job_position_id: job_position_id,
          status: status
        }).where({ id: id });
        res.status(200).json({ erro: false, msg: "Cadastro Atualizado Com Sucesso!!!" });
      } catch (error) {
        res.status(200).json({ erro: true, msg: error.message });
      }
    }
  },

  async destroy(req, res) {
    const { id, password } = req.body;
    if (!password) {
      res.status(400).json({ erro: "Enviar Senha." });
      return;
    }
    try {
      const usuario = await knex("user").where({ id });
      if (bcrypt.compareSync(password, usuario[0].password)) {
        await knex("user").del().where({ id });
        res.status(200).json({ erro: false, msg: "Usuario Deletado" });
      } else {
        res.status(400).json({ erro: true, msg: "Senha incorreta" });
      }
    } catch (error) {
      res.status(400).json({ erro: true, msg: "Usuario Não Encontrado!" });
    }
  },

};

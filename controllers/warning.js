const knex = require("../dbConfig");

module.exports = {
    async show(req, res) {
        const data = await knex("warning")
            .select(
                "w.id as idwarning",
                "w.title",
                "w.description",
                "w.weight",
                "w.created_at",
            )
            .from("warning as w")
            .orderBy('idwarning', "desc")
        res.status(200).json({ warnings: data });
    },
    async createwarning(req, res) {
        const { title, description, weight } = req.body;
        if (!title || !description || !weight) {
            res.status(400).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            await knex("warning").insert({
                title,
                description,
                weight
            });
            res.status(200).json({ erro: false, msg: "Seu Aviso foi Salvo!" });
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },
    async editWarning(req, res) {
        const { id, title, description, weight } = req.body;
        if (!id || !title || !description || !weight) {
            res.status(400).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            const warning = await knex("warning").where({ id: id });
            if (warning.length) {
                await knex("warning")
                    .update({ title: title, description: description, weight: weight })
                    .where({ id: id });
                res.status(200).json({ erro: false, msg: "Seu Aviso foi Atualizado!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },
    async destroy(req, res) {
        const { id } = req.params;
        try {
            const warning = await knex("warning").where({ id: id });
            if (warning.length) {
                await knex("warning").del().where({ id: id });
                res.status(200).json({ erro: false, msg: "Aviso Removido" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: "Aviso Não Encontrado!" });
        }
    },
}
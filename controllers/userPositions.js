const knex = require("../dbConfig");

module.exports = {

    async show(req, res) {
        const { user_id, date } = req.body
        if (user_id == 0) {
            const data = await knex("positions").select(
                "p.id",
                "p.lat",
                "p.long",
                "u.name",
                "u.job_position_id",
                "j.position",
                "p.speed",
                "p.user_id",
                "p.created_at",
                "p.updated_at"
            ).from("positions as p")
                .leftJoin("user as u", "p.user_id", "u.id")
                .leftJoin("job_position as j", "u.job_position_id", "j.id");
            res.status(200).json({ data: data });
        } else {
            const data = await knex("positions").select(
                "p.id",
                "p.lat",
                "p.long",
                "u.name",
                "u.job_position_id",
                "j.position",
                "p.speed",
                "p.user_id",
                "p.created_at",
                "p.updated_at"
            ).from("positions as p")
                .leftJoin("user as u", "p.user_id", "u.id")
                .leftJoin("job_position as j", "u.job_position_id", "j.id")
                .where({ user_id })
                .where('p.created_at', '>=', `${date}T00:00:00.000Z`)
                .where('p.created_at', '<', `${date}T23:59:59.000Z`)
            res.status(200).json({ data: data });
        }
    },

    async showLastPositions(req, res) {
        const data = await knex("last_positions").select(
            "p.id",
            "p.lat",
            "p.long",
            "u.name",
            "u.job_position_id",
            "j.position",
            "p.speed",
            "p.user_id",
            "p.created_at",
            "p.updated_at"
        ).from("last_positions as p")
            .leftJoin("user as u", "p.user_id", "u.id")
            .leftJoin("job_position as j", "u.job_position_id", "j.id");
        res.status(200).json({ data: data });
    },

    async store(req, res) {
        const { user_id, lat, long, speed } = req.body;
        if (!user_id || !lat || !long || !speed) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        try {
            const oldPositions = await knex("last_positions").select("id", "user_id").where({ user_id: user_id })

            if (oldPositions.length) {
                await knex("last_positions").update({ lat: lat, long: long, speed: speed }).where({ id: oldPositions[0].id })
            } else {
                await knex("last_positions").insert({ lat: lat, long: long, speed: speed, user_id: user_id })
            }
            await knex("positions").insert({ lat: lat, long: long, speed: speed, user_id: user_id });

            res.status(200).json({ erro: false, msg: "Posição Cadastrada Com Sucesso!" });
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    },
}
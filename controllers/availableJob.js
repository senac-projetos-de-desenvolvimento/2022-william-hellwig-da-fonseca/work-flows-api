const knex = require("../dbConfig");

module.exports = {
    async show(req, res) {
        const data = await knex("available_job")
            .select(
                "a.id as availablejob_id",
                "j.id",
                "j.position",
                "a.message",
                "a.created_at",
            )
            .from("available_job as a")
            .leftJoin('job_position as j', 'a.job_position_id', 'j.id')
            .orderBy('a.created_at', 'desc')

        res.status(200).json({ data: data });
    },
    async createVacancie(req, res) {
        const { idUser, idJob, message } = req.body;
        if (!idUser || !idJob || !message) {
            res.status(400).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            await knex("available_job").insert({
                user_id: idUser,
                job_position_id: idJob,
                message: message,
            });
            res.status(200).json({ erro: false, msg: "Nova Vaga Cadastrada!" });
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },
    async editVacancie(req, res) {
        const { id, idJob, message } = req.body;
        if (!id || !idJob || !message) {
            res.status(400).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            const vacancie = await knex("available_job").where({ id: id });
            if (vacancie.length) {
                await knex("available_job").update({ message: message, job_position_id: idJob }).where({ id: id });
                res.status(200).json({ erro: false, msg: "Vaga Atualizada!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },
    async destroy(req, res) {
        const { id } = req.params;
        console.log(id)
        try {
            const vacancie = await knex("available_job").where({ id: id });
            if (vacancie.length) {
                await knex("available_job").del().where({ id: id });
                res.status(200).json({ erro: false, msg: "Vaga Removida!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: "Vaga Não localizada!" });
        }
    },
}
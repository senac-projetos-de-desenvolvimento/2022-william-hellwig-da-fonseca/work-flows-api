const knex = require("../dbConfig");

module.exports = {
    async createComment(req, res) {
        const { idUser, idPublication, comment, score } = req.body;
        const id = idUser

        if (!idUser || !idPublication || !comment || !score) {
            res.status(400).json({ erro: true, msg: "Envie Corretamente os Dados!!!" });
            return;
        }
        try {
            await knex("comment").insert({
                user_id: idUser,
                publication_id: idPublication,
                comment,
            });
            await knex("user").update("score", score).where({ id });
            res.status(200).json({ erro: false, msg: "Comentario Salvo!" });
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },
}
const knex = require("../dbConfig");

module.exports = {

    async show(req, res) {
        const data = await knex("company_location").select(
            "id",
            "lat",
            "lon",
            "distance",
            "created_at",
            "updated_at"
        )
        res.status(200).json({ data: data });
    },
    async store(req, res) {
        const { lat, lon, distance } = req.body;
        if (!lat || !lon || !distance) {
            res.status(200).json({ erro: true, msg: "Enviar dados Corretamente!" });
        }
        try {
            const location = await knex("company_location")
                .update({ lat: lat, lon: lon, distance: distance })
                .where({ id: 1 })
            res.status(200).json({ erro: false, msg: "Localização Atualizada com Sucesso!!!" });
        }
        catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    }
}

const knex = require("../dbConfig");

module.exports = {

    async show(req, res) {
        const data = await knex("job_position").select(
            "j.id",
            "j.position",
            "j.access_level_id",
            "j.created_at",
            "j.updated_at"
        )
            .from("job_position as j")
            .leftJoin("access_level as a", "j.access_level_id", "a.id");
        res.status(200).json({ erro: false, data: data });
    },

    async createJob(req, res) {
        const { position, access_level_id } = req.body;
        if (!position || !access_level_id) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        try {
            const data = await knex("job_position").where({ position: position });
            if (data.length) {
                res.status(400).json({ erro: true, msg: "Cargo Já Cadastrado!" });
                return;
            } else {
                await knex("job_position").insert({ position: position, access_level_id: access_level_id });
                res.status(200).json({ erro: false, msg: "Cargo Cadastrado Com Sucesso!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },

    async editJob(req, res) {
        const { id, position, access_level_id } = req.body;
        if (!id || !position || !access_level_id) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
            return;
        }
        try {
            const valPosition = await knex("job_position").where({ id: id });
            if (valPosition.length) {
                await knex("job_position").update({ position: position, access_level_id: access_level_id }).where({ id: id });
                res.status(200).json({ erro: false, msg: "Cargo Atualizado Com Sucesso!" });
            }
        } catch (error) {
            res.status(400).json({ erro: true, msg: error.message });
        }
    },

    async destroyJob(req, res) {
        const { id } = req.params;
        if (!id) {
            res.status(200).json({ erro: true, msg: "Enviar Dados Corretamente!" });
        }
        try {
            const valPosition = await knex("job_position").where({ id: id });
            const valUser = await knex("user").where({ job_position_id: valPosition[0].id });
            if (valUser.length) {
                res.status(200).json({ erro: true, msg: "Existem Usuarios Com Esse Cargo!" });
            } else {
                if (valPosition.length) {
                    await knex("job_position").del().where({ id: id });
                    res.status(200).json({ erro: false, msg: "Cargo Removido Com Sucesso!" });
                }
            }
        } catch (error) {
            res.status(200).json({ erro: true, msg: error.message });
        }
    },
}

# Work Flows Api :globe_with_meridians:

## Descrição :memo:
Api NodeJs desenvolvida para servir a aplicações mobile ou Web.

## Instalando :hammer:

> npm install 

## Rodando Aplicação :rocket:
>- node Server.js
>- nodemon Server.js
>- pm2 start Server.js


## Status :white_check_mark: 
Finalizado!
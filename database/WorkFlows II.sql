/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 8.0.31-0ubuntu0.22.04.1 : Database - work_flows
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`work_flows` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `work_flows`;

/*Table structure for table `access_level` */

DROP TABLE IF EXISTS `access_level`;

CREATE TABLE `access_level` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `level` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

/*Data for the table `access_level` */

insert  into `access_level`(`id`,`description`,`level`,`created_at`,`updated_at`) values (1,'Usuario',1,'2022-09-23 01:02:16','2022-09-26 10:20:06'),(2,'Moderador',3,'2022-09-26 10:46:15','2022-10-28 08:57:33'),(3,'Gerente',5,'2022-10-28 08:56:19','2022-10-28 08:59:14'),(4,'RH',7,'2022-10-28 08:59:44','2022-10-28 08:59:44'),(5,'Administrador',9,'2022-10-28 09:00:34','2022-10-28 09:00:34'),(6,'Master',10,'2022-10-28 09:01:09','2022-10-28 09:01:09');

/*Table structure for table `available_job` */

DROP TABLE IF EXISTS `available_job`;

CREATE TABLE `available_job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  `job_position_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_available_jobs_user1_idx` (`user_id`),
  KEY `fk_available_job_job_position1_idx` (`job_position_id`),
  CONSTRAINT `fk_available_job_job_position1` FOREIGN KEY (`job_position_id`) REFERENCES `job_position` (`id`),
  CONSTRAINT `fk_available_jobs_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

/*Data for the table `available_job` */

insert  into `available_job`(`id`,`message`,`created_at`,`updated_at`,`user_id`,`job_position_id`) values (1,'Funções: Receber, conferir e armazenar mercadorias. Reposição de mercadorias para vendas','2022-07-12 18:34:23','2022-07-12 18:34:23',7,15),(2,'Atendimento ao cliente, organização da loja, reposição de mercadorias.','2022-07-12 18:37:46','2022-07-12 18:37:46',7,7),(5,'Vaga Aberta.','2022-10-31 12:01:55','2022-10-31 12:20:59',6,14);

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `comment` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `publication_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_publications1_idx` (`publication_id`),
  KEY `fk_comments_user1_idx` (`user_id`),
  CONSTRAINT `fk_comments_publications1` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`),
  CONSTRAINT `fk_comments_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;

/*Data for the table `comment` */

insert  into `comment`(`id`,`comment`,`created_at`,`updated_at`,`publication_id`,`user_id`) values (8,'Gostamos da ideia, iremos analisar formas de ','2022-07-12 18:56:20','2022-07-12 18:56:20',25,7),(9,'Acho interessante essa ideia, assim os client','2022-07-12 21:44:39','2022-07-12 21:44:39',26,20),(10,'Será para todos os setores estes cursos?','2022-07-12 21:51:50','2022-07-12 21:51:50',28,22),(11,'Sim. Todos os colaboradores terão a oportunid','2022-07-12 21:53:02','2022-07-12 21:53:02',28,16),(12,'Ótima sugestão! Facilitaria a identificação d','2022-07-19 22:01:22','2022-07-19 22:01:22',26,6),(13,'Está sujestão será analisada e em breve imple','2022-07-20 17:21:51','2022-07-20 17:21:51',25,6);

/*Table structure for table `company_location` */

DROP TABLE IF EXISTS `company_location`;

CREATE TABLE `company_location` (
  `id` int NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `distance` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

/*Data for the table `company_location` */

insert  into `company_location`(`id`,`lat`,`lon`,`distance`,`created_at`,`updated_at`) values (1,-31.755772986212346,-52.30392986816441,50,'2022-10-06 17:18:27','2022-10-28 16:43:27');

/*Table structure for table `job_position` */

DROP TABLE IF EXISTS `job_position`;

CREATE TABLE `job_position` (
  `id` int NOT NULL AUTO_INCREMENT,
  `position` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_level_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_position_access_level1_idx` (`access_level_id`),
  CONSTRAINT `fk_job_position_access_level1` FOREIGN KEY (`access_level_id`) REFERENCES `access_level` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3;

/*Data for the table `job_position` */

insert  into `job_position`(`id`,`position`,`created_at`,`updated_at`,`access_level_id`) values (4,'Administrador','2022-07-12 01:02:16','2022-10-28 20:40:42',5),(6,'Gerente de Vendas','2022-07-12 17:55:54','2022-10-28 20:41:34',3),(7,'Vendedor (a)','2022-07-12 17:58:39','2022-07-12 17:58:39',1),(8,'Caixa','2022-07-12 17:59:52','2022-07-12 17:59:52',1),(9,'Auxiliar Administrativo ','2022-07-12 18:00:50','2022-10-28 20:41:44',2),(10,'Financeiro ','2022-07-12 18:02:26','2022-10-28 20:42:21',5),(11,'Recursos Humanos','2022-07-12 18:03:43','2022-10-28 20:41:15',4),(12,'Marketing ','2022-07-12 18:04:13','2022-07-12 18:04:13',1),(13,'Crediário ','2022-07-12 18:04:35','2022-07-12 18:04:35',1),(14,'Limpeza','2022-07-12 18:05:01','2022-07-12 18:05:01',1),(15,'Estoquista','2022-07-12 18:28:58','2022-07-12 18:28:58',1);

/*Table structure for table `positions` */

DROP TABLE IF EXISTS `positions`;

CREATE TABLE `positions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `speed` double NOT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_positions_user1_idx` (`user_id`),
  CONSTRAINT `fk_positions_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb3;

/*Data for the table `positions` */

insert  into `positions`(`id`,`speed`,`lat`,`long`,`updated_at`,`created_at`,`user_id`) values (1,0,-31.7557087,-52.3036885,'2022-10-27 18:05:55','2022-10-27 18:05:55',6),(2,0,-31.755738,-52.3035989,'2022-10-27 18:06:34','2022-10-27 18:06:34',6),(3,0,-31.7557483,-52.3035917,'2022-10-27 18:07:04','2022-10-27 18:07:04',6),(4,10.3,-31.7569067,-52.3064067,'2022-10-27 18:08:31','2022-10-27 18:08:31',6),(5,12.32,-31.7574483,-52.3092117,'2022-10-27 18:14:06','2022-10-27 18:14:06',6),(6,9.94,-31.7357233,-52.3254867,'2022-10-27 18:16:05','2022-10-27 18:16:05',6),(7,14.46,-31.7345617,-52.33338,'2022-10-27 18:16:26','2022-10-27 18:16:26',6),(8,12.33,-31.7332017,-52.3363117,'2022-10-27 18:16:56','2022-10-27 18:16:56',6),(9,13.47,-31.7319042,-52.3397949,'2022-10-27 18:17:26','2022-10-27 18:17:26',6),(10,1.07,-31.731075,-52.341595,'2022-10-27 18:17:56','2022-10-27 18:17:56',6),(11,12.39,-31.7292894,-52.3411988,'2022-10-27 18:18:26','2022-10-27 18:18:26',6),(12,11.65,-31.7262237,-52.3397694,'2022-10-27 18:18:56','2022-10-27 18:18:56',6),(13,13.69,-31.7232946,-52.3377794,'2022-10-27 18:19:26','2022-10-27 18:19:26',6),(14,7.85,-31.720332,-52.335964,'2022-10-27 18:19:56','2022-10-27 18:19:56',6),(15,11.87,-31.7188612,-52.3390715,'2022-10-27 18:20:26','2022-10-27 18:20:26',6),(16,10.24,-31.7172612,-52.3425361,'2022-10-27 18:20:56','2022-10-27 18:20:56',6),(17,8.56,-31.7154717,-52.3426617,'2022-10-27 18:21:26','2022-10-27 18:21:26',6),(18,4.05,-31.713386,-52.3423911,'2022-10-27 18:21:56','2022-10-27 18:21:56',6),(19,4.25,-31.712824,-52.3424316,'2022-10-27 18:22:26','2022-10-27 18:22:26',6),(20,8.3,-31.7109223,-52.3445452,'2022-10-27 18:22:56','2022-10-27 18:22:56',6),(21,7.76,-31.7097857,-52.3456166,'2022-10-27 18:23:26','2022-10-27 18:23:26',6),(22,3.25,-31.7096792,-52.3450353,'2022-10-27 18:23:56','2022-10-27 18:23:56',6),(23,0,-31.709749,-52.3448649,'2022-10-27 18:24:30','2022-10-27 18:24:30',6),(24,0,-31.7144703,-52.3374851,'2022-10-28 08:13:39','2022-10-28 08:13:39',6),(25,0.84,-31.709952,-52.3449357,'2022-10-28 08:15:01','2022-10-28 08:15:01',6),(26,0,-31.7099633,-52.3449318,'2022-10-28 08:15:31','2022-10-28 08:15:31',6),(27,0.07,-31.7099543,-52.3449408,'2022-10-28 08:21:42','2022-10-28 08:21:42',6),(28,0,-31.7097517,-52.3449762,'2022-10-28 08:23:43','2022-10-28 08:23:43',6),(29,0,-31.7164029,-52.3429631,'2022-10-28 08:24:09','2022-10-28 08:24:09',6),(30,0,-31.7172615,-52.3422497,'2022-10-28 08:24:41','2022-10-28 08:24:41',6),(31,0,-31.7195493,-52.3378707,'2022-10-28 08:25:11','2022-10-28 08:25:11',6),(32,0,-31.7226999,-52.3376088,'2022-10-28 08:25:42','2022-10-28 08:25:42',6),(33,0,-31.7267926,-52.3398524,'2022-10-28 08:26:12','2022-10-28 08:26:12',6),(34,0,-31.7273983,-52.34069,'2022-10-28 08:26:43','2022-10-28 08:26:43',6),(35,0,-31.7319277,-52.3386342,'2022-10-28 08:27:13','2022-10-28 08:27:13',6),(36,0,-31.7334518,-52.3356454,'2022-10-28 08:27:43','2022-10-28 08:27:43',6),(37,0,-31.7348301,-52.3320099,'2022-10-28 08:28:14','2022-10-28 08:28:14',6),(38,0,-31.7370094,-52.3277667,'2022-10-28 08:28:44','2022-10-28 08:28:44',6),(39,0,-31.7380026,-52.3279965,'2022-10-28 08:29:14','2022-10-28 08:29:14',6),(40,0,-31.7383205,-52.3267368,'2022-10-28 08:29:45','2022-10-28 08:29:45',6),(41,0,-31.7401127,-52.3247532,'2022-10-28 08:30:15','2022-10-28 08:30:15',6),(42,11.62,-31.7431133,-52.3214933,'2022-10-28 08:30:46','2022-10-28 08:30:46',6),(43,0,-31.7473885,-52.3169491,'2022-10-28 08:31:31','2022-10-28 08:31:31',6),(44,0,-31.7487364,-52.3152619,'2022-10-28 08:33:43','2022-10-28 08:33:43',6),(45,10.12,-31.75255,-52.3067767,'2022-10-28 08:33:54','2022-10-28 08:33:54',6),(46,12.9,-31.755545,-52.303145,'2022-10-28 08:34:24','2022-10-28 08:34:24',6),(47,0,-31.7555433,-52.303265,'2022-10-28 08:34:58','2022-10-28 08:34:58',6),(48,0,-31.755655,-52.30367,'2022-10-28 08:35:28','2022-10-28 08:35:28',6),(49,0,-31.7557,-52.3035433,'2022-10-28 08:36:00','2022-10-28 08:36:00',6),(50,1.31,-31.7557517,-52.3042133,'2022-10-28 08:36:44','2022-10-28 08:36:44',6),(51,0,-31.7555717,-52.3043267,'2022-10-28 08:39:02','2022-10-28 08:39:02',6),(52,0,-31.7557089,-52.3036959,'2022-10-28 17:59:30','2022-10-28 17:59:30',6),(53,0,-31.7556341,-52.3037023,'2022-10-28 18:00:06','2022-10-28 18:00:06',6),(54,0.43,-31.7556832,-52.3037514,'2022-10-28 18:00:34','2022-10-28 18:00:34',6),(55,0,-31.7556768,-52.3037494,'2022-10-28 18:01:10','2022-10-28 18:01:10',6),(56,0,-31.7556795,-52.3036637,'2022-10-28 18:02:03','2022-10-28 18:02:03',6),(57,0,-31.7556973,-52.3036424,'2022-10-28 18:02:34','2022-10-28 18:02:34',6),(58,0.85,-31.7555401,-52.30331,'2022-10-28 18:08:27','2022-10-28 18:08:27',6),(59,5.59,-31.7388402,-52.3261144,'2022-10-28 18:08:33','2022-10-28 18:08:33',6),(60,0.19,-31.7383615,-52.3269225,'2022-10-28 18:18:36','2022-10-28 18:18:36',6),(61,12.13,-31.7165486,-52.3428051,'2022-10-28 18:26:46','2022-10-28 18:26:46',6),(62,0,-31.7651761,-52.2278196,'2022-10-29 20:23:06','2022-10-29 20:23:06',6),(63,0,-31.7651872,-52.2276221,'2022-10-29 20:23:38','2022-10-29 20:23:38',6),(64,0,-31.7637807,-52.2283869,'2022-10-29 20:24:14','2022-10-29 20:24:14',6),(65,0,-31.761932,-52.2284909,'2022-10-29 20:24:44','2022-10-29 20:24:44',6),(66,0.6,-31.7618821,-52.2282066,'2022-10-29 20:25:10','2022-10-29 20:25:10',6),(67,5.88,-31.7615188,-52.2298258,'2022-10-29 20:25:44','2022-10-29 20:25:44',6),(68,8.93,-31.761557,-52.2323005,'2022-10-29 20:26:13','2022-10-29 20:26:13',6),(69,8.38,-31.7614126,-52.2349295,'2022-10-29 20:31:32','2022-10-29 20:31:32',6),(70,16.8,-31.7588201,-52.2602792,'2022-10-29 20:41:51','2022-10-29 20:41:51',6),(71,8.81,-31.7561184,-52.3044428,'2022-10-29 20:57:10','2022-10-29 20:57:10',6),(72,10.28,-31.7203226,-52.3478458,'2022-10-29 20:57:14','2022-10-29 20:57:14',6),(73,5.57,-31.7187106,-52.3506024,'2022-10-29 20:57:50','2022-10-29 20:57:50',6),(74,0,-31.7165419,-52.3518081,'2022-10-29 20:58:44','2022-10-29 20:58:44',6),(75,0.78,-31.7159771,-52.351542,'2022-10-29 20:59:13','2022-10-29 20:59:13',6),(76,0.82,-31.7160663,-52.3515379,'2022-10-29 20:59:52','2022-10-29 20:59:52',6),(77,0,-31.7160113,-52.3514407,'2022-10-29 21:00:23','2022-10-29 21:00:23',6),(78,1.18,-31.7160775,-52.3516491,'2022-10-29 21:06:27','2022-10-29 21:06:27',6),(79,1.41,-31.7159695,-52.3514039,'2022-10-29 21:06:30','2022-10-29 21:06:30',6);

/*Table structure for table `publication` */

DROP TABLE IF EXISTS `publication`;

CREATE TABLE `publication` (
  `id` int NOT NULL AUTO_INCREMENT,
  `publication` varchar(3000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_publications_user1_idx` (`user_id`),
  CONSTRAINT `fk_publications_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb3;

/*Data for the table `publication` */

insert  into `publication`(`id`,`publication`,`created_at`,`updated_at`,`user_id`) values (25,'Sugestão de criação Espaço Kids\nOnde os pais possam deixar seus filhos se divertindo nos momentos de compra.','2022-07-12 18:54:38','2022-07-12 18:54:38',10),(26,'Sugestão de cores diversificadas para cada setor ','2022-07-12 21:43:28','2022-07-12 21:43:28',13),(27,'Iremos colocar duas caixinha de sugestões e reclamações afim de melhor nosso atendimento e convívio entre colaboradores.','2022-07-12 21:47:10','2022-07-12 21:47:10',15),(28,'Teremos em nossa empresa um programa de bônus para nossos colaboradores, onde que serão disponibilizados cursos onlines e ao final de cada curso o colaborador poderá criar um desafio e até mesmo propor uma nova forma de funcionamento do estabelecimento e se for aceito terá uma recompensa.','2022-07-12 21:51:02','2022-07-12 21:51:02',16);

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `type` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `publication_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reports_publication1_idx` (`publication_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;

/*Data for the table `reports` */

/*Table structure for table `suggestion` */

DROP TABLE IF EXISTS `suggestion`;

CREATE TABLE `suggestion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `suggestion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `suggestion_UNIQUE` (`suggestion`),
  KEY `fk_suggestions_user1_idx` (`user_id`),
  CONSTRAINT `fk_suggestions_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

/*Data for the table `suggestion` */

insert  into `suggestion`(`id`,`suggestion`,`created_at`,`updated_at`,`user_id`) values (2,'Quadro de colaboradores do mês ','2022-07-12 18:59:03','2022-07-12 18:59:03',11),(3,'Criar forma de pagamento via QR Code, onde clientes possam pagar com mais facilidade.','2022-07-12 19:01:53','2022-07-12 19:01:53',19),(4,'Criar uma semana de limpa estoque, estamos com muitas peças paradas sem saída de venda','2022-07-12 22:06:45','2022-07-12 22:06:45',23);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `image` varchar(1000) NOT NULL DEFAULT '/images/User.png',
  `email` varchar(45) DEFAULT NULL,
  `fone` varchar(20) DEFAULT NULL,
  `ctps` varchar(14) NOT NULL,
  `password` varchar(500) NOT NULL,
  `location` tinyint NOT NULL DEFAULT '0',
  `score` int NOT NULL DEFAULT '400',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `job_position_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`job_position_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_user_job_position1_idx` (`job_position_id`),
  CONSTRAINT `fk_user_job_position1` FOREIGN KEY (`job_position_id`) REFERENCES `job_position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`image`,`email`,`fone`,`ctps`,`password`,`location`,`score`,`created_at`,`updated_at`,`job_position_id`,`status`) values (6,'Willian H. da Fonseca','/uploads/f4bd86efa842552000708076d6458620','will@workflows.com','5398145868','4323224358974','$2b$10$wm6yTBevOdRbtmxsivgPieEwrAQTUgXpKFHlZPrfTGiCWQVR5EvyC',1,478,'2022-07-12 01:06:15','2022-10-28 12:16:54',4,1),(7,'Carina H. Stallbaum','/images/User.png','carina@workflows.com','984584984','178.40826.78-3','$2b$10$b29bB.zchIJHicRaV7dmxutMrshnWrOs6qnduadaEgvD7rFjCymyq',0,100,'2022-07-12 02:21:58','2022-10-28 12:15:38',4,1),(8,'Luiza Camargo','/images/User.png','luizacamargo@workflows.com','53999999999','3420895840','$2b$10$xJRkbtKeZTzU6jGdjZ13s.YxFnriSVRDNKPnE3LBbd9w/Gu/vEiuq',1,100,'2022-07-12 18:08:07','2022-10-28 12:15:38',6,1),(9,'Fernando Luis Ramalho','/images/User.png','fernandoluis@workflows.com','53999999999','1156749048','$2b$10$gOQS/ZXia46gkQ5casaXK.8zhxE61MDhw49tn4JE/MEhWZOC3hpWG',0,100,'2022-07-12 18:09:21','2022-10-28 12:15:38',7,1),(10,'Helena Ribes','/images/User.png','helena@workflows.com','53999999999','1134527093','$2b$10$75ftEiX4piu.3lX396TDA.bek1bShpEkQkUh8RkcDl/J/pU8a5uTi',0,100,'2022-07-12 18:11:27','2022-10-28 12:15:38',7,1),(11,'Taine Lima','/images/User.png','Taine@workflows.com','53999999999','1156329658','$2b$10$Uy/3USJZojl4vSf.7jMvweq2QJCGk2vq.wkjxGajdvwgoLeuBk52e',0,100,'2022-07-12 18:12:46','2022-10-28 12:15:38',7,1),(12,'Junior Castro','/images/User.png','junior@workflows.com','53999999999','1145628904','$2b$10$EK5XFg8RKB7W1V5iJs6D/uaoN1RmTJCX9nBZQd1f677mjkthdqyha',1,100,'2022-07-12 18:13:43','2022-10-28 12:15:38',7,1),(13,'Erick Lopes','/images/User.png','erick@workflows.com','53999999999','1167867540','$2b$10$CkMQqNsa71zihqaZ02aTE.W6r/Chqg4iVBYfr93VID.kSqY6ruUE6',0,100,'2022-07-12 18:14:38','2022-10-28 12:15:38',8,1),(14,'Rafaela Peter','/images/User.png','Rafaela@workflows','53999999999','1156478965','$2b$10$HHhgScJfd2zI9qS62hBZReAaJOoabp4S2ysyspWJ7/eL3s.U3Owtm',0,100,'2022-07-12 18:15:28','2022-10-28 12:15:38',8,1),(15,'Fernanda Pagel','/images/User.png','fernanda@workflows.com','53999999999','214568747','$2b$10$h9WDUFth7u./drozF8vQT.ykpj6wF6NPTFnKDQk58HDhlD/n6CcQi',0,100,'2022-07-12 18:16:41','2022-10-28 12:15:38',9,1),(16,'Luciano Harter','/images/User.png','luciano@workflows.com','53999999999','2135697409','$2b$10$wZHMm5Lv0idKkICCWIFzy./p7DtUEZoMjZkT1AiTw0/DesWYUDvyi',0,100,'2022-07-12 18:17:46','2022-10-28 12:15:38',10,1),(17,'Tereza Silva','/images/User.png','tereza@workflows.com','53999999999','1145328940','$2b$10$xTTLSAvNkeVZKE.a1pg8a.kW387YcjlIRFdY/bF2rt9Ija/M421hy',0,100,'2022-07-12 18:18:59','2022-10-28 12:15:38',11,1),(18,'Matheus Fonseca','/images/User.png','matheus@workflows.com','53999999999','1189074747','$2b$10$0XNhdZimmsVUoMoEABjwI.07IOsT6ldbP.0x3aubKICQrZ7RZGLcO',0,100,'2022-07-12 18:20:23','2022-10-28 12:15:38',12,1),(19,'Betina Lindemann','/images/User.png','betina@workflows.com','53999999999','115673803','$2b$10$izJ/chB5sIKx7jS1K5fayOlIdDPJHY.i5GLVerKFSgt3.FfY.DFpC',0,100,'2022-07-12 18:21:24','2022-10-28 12:15:38',13,1),(20,'Raissa Macedo','/images/User.png','raissa@workflows.com','53999999999','1123456758','$2b$10$dZ3r/E3pRC98rYMp4p0ft.n.zr10FTJK6w7ORQmbSGop0WgarbztS',0,100,'2022-07-12 18:22:41','2022-10-28 12:15:38',13,1),(21,'Germana Santos','/images/User.png','germana@workflows.com','53999999999','1156794830','$2b$10$pcyNlnPPJKIJgHSBpOYLceFI5g2Pp6kkjDHi3Ks8LD2yQQfj9/BjS',0,100,'2022-07-12 18:23:53','2022-10-28 12:15:38',14,1),(22,'Cristiane Lopes ','/images/User.png','Cristiane@workflows.com','53999999999','1156735678','$2b$10$D/mjOa53rLJLe9N6Dfs4Nex68.uMy4UC0ueEAtM2SBhqle9J0YXlK',0,100,'2022-07-12 18:25:06','2022-10-28 12:15:38',14,1),(23,'Josiane Kuster','/images/User.png','josiane@workflows.com','53999999999','1108967846','$2b$10$WMlbX8pmh1I.QCFf14Rn5eVCx0ceCEMtTavlXiSLZMmcEKpKak68q',0,100,'2022-07-12 18:30:16','2022-10-28 12:15:38',15,1),(24,'Daniel Hellwig ','/images/User.png','daniel@workflows.com','53999999999','114256487','$2b$10$V2HFzs1jtHbfGZrhAbgfRePYKD0re2et1CW92jPiX5OXViux0SS.O',0,100,'2022-07-12 18:31:03','2022-10-28 12:15:38',15,1),(25,'Paloma Blank','/images/User.png','paloma@workflows.com','53999999999','1189537894','$2b$10$WZNdGdLpR3wnhJs.hqeoyuLe16tB1NPj4rlJ7Lu/YItmqIwXuq8e6',0,100,'2022-07-12 18:31:53','2022-10-28 12:15:38',15,1),(26,'Gladimir','/images/User.png','Gladimir@workflows.com','981234567','9123576542','$2b$10$VqkI.3HJRhSM27J1fQ.Mz.06d7Ived.0KUv.4BbJ/mjuQtRoLL7zC',0,100,'2022-07-20 23:00:44','2022-10-28 12:15:38',10,1),(30,'willian H. Da Fonseca','/images/User.png','test@teste.com','53981490055','8212897189273','$2b$10$XujP8OK2WLDeALoL.jxnQOv8BXZ/J/sR4Dy7ftsfB5pyidzsCQVuy',0,100,'2022-10-07 15:12:00','2022-10-28 12:15:38',6,1);

/*Table structure for table `warning` */

DROP TABLE IF EXISTS `warning`;

CREATE TABLE `warning` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(500) NOT NULL,
  `weight` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;

/*Data for the table `warning` */

insert  into `warning`(`id`,`title`,`description`,`weight`,`created_at`,`updated_at`) values (18,'Dia dos Pais','Boa tarde colaboradores!\nNo dia 13 de agosto véspera Dia dos Pais não haverá fechamento ao meio dia. Iremos ter revezamentos entre os vendedores para que assim nossos clientes possam ser bem  atendidos.\nSendo assim será disponibilizado almoço a todos os colaboradores.\n\nAgradeço a compreensão de todos.',3,'2022-07-12 18:43:56','2022-07-12 18:43:56'),(19,'Batida do Ponto ','Bom dia colaboradores!\nVenho pedir a atenção de todos para o batimento de seu Horário Ponto. Não esqueçam de informar o horário de saída para o almoço e retorno.\n\nAgradeço a compreensão.',2,'2022-07-12 18:49:21','2022-07-12 18:49:21'),(20,'Urgente ','Colaboradores!\nNesta semana teremos um profissional da saúde em nossa empresa, afim de realizar a vacina contra a Influenza, pois como todos estão cientes pelo surto de contaminação entre nossa equipe do escritório.\n\nPeço que por gentileza todos tragam sua carteira de vacinação para estarmos todos em dia.\n\nUma boa noite e bom descanso.',1,'2022-07-12 22:03:57','2022-07-12 22:03:57');

/*Table structure for table `work_point` */

DROP TABLE IF EXISTS `work_point`;

CREATE TABLE `work_point` (
  `id` int NOT NULL AUTO_INCREMENT,
  `workpoint` tinyint NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_work_point_user1_idx` (`user_id`),
  CONSTRAINT `fk_work_point_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

/*Data for the table `work_point` */

insert  into `work_point`(`id`,`workpoint`,`created_at`,`updated_at`,`user_id`) values (1,0,'2022-10-18 20:08:36','2022-10-18 20:08:36',6),(2,1,'2022-10-18 20:09:28','2022-10-18 20:09:28',6);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

const express = require('express')
const Route = require('./routes/routes')
const app = express()
const port = process.env.PORT || 2606

app.get('/', (req, res) => {
  res.send(`Server Runing Port:${port}`)
})

app.use(express.json())
app.use(Route)

app.use(express.static('public'));

app.listen(port, () => {
  
  console.log(`Server Runing http://localhost:${port}`)
})
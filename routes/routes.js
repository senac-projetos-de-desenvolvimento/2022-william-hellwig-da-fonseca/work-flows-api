const express = require("express");
const cors = require('cors')
const Route = express.Router();

Route.use(express.static('public'));
Route.use(cors())

const verifyLogin = require("../middlewares/login");

const companyController = require("../controllers/companyLocation");
const availableController = require("../controllers/availableJob");
const positionController = require("../controllers/userPositions");
const suggestionController = require("../controllers/suggestion");
const jobController = require("../controllers/jobPositions");
const commentController = require("../controllers/comment");
const warningController = require("../controllers/warning");
const pointController = require("../controllers/workPoint");
const accessController = require("../controllers/access");
const reportController = require("../controllers/report");
const imageController = require("../controllers/image");
const userController = require("../controllers/user");
const feedController = require("../controllers/feed");
const login = require("../controllers/login");

Route
  // Rota de login
  .post("/login", login.index) // Login

Route
  // Rotas de controle de Usuarios.
  .get("/user/:token", verifyLogin, userController.show) // Lista de usuarios
  .get("/score/:token", verifyLogin, userController.getScore) // Lista de usuarios por score
  .post("/user/:token", verifyLogin, userController.createUser) // Cadastra um usuario
  .post("/altimg/:token", verifyLogin, imageController.imagePost) // altera senha do usuario
  .put("/userupscore/:token", verifyLogin, userController.updateScore) // altera os pontos
  .post("/userup/:token", verifyLogin, userController.update) // altera senha do usuario
  .post("/userupjob/:token", verifyLogin, userController.upJobPosition) // Altera o Cargo de trabalho 
  .delete("/userdel/:token", verifyLogin, userController.destroy) // remove o usuario

Route
  // Rotas de Controle de Cargos
  .get("/job/:token", verifyLogin, jobController.show) // lista Todos os Cargos Cadastrados
  .post("/job/:token", verifyLogin, jobController.createJob) // Cadastro De Cargos
  .put("/jobedit/:token", verifyLogin, jobController.editJob) // Edição De Cargos
  .delete("/delljob/:token/:id", verifyLogin, jobController.destroyJob) // Remoção De Cargos

Route
  // Rotas de Controle de acesso
  .get("/access/:token", verifyLogin, accessController.show) // lista Todos os acessos
  .post("/access/:token", verifyLogin, accessController.store) // Cadastro acessos

Route
  // Rota de Controle das Publicações
  .get("/feed/:token", verifyLogin, feedController.show) // lista de Todas as Questões
  .post("/feed/:token", verifyLogin, feedController.createPublic) // Cadastro de Questões

Route
  // Rota de Controle dos Comentarios
  .post("/comment/:token", verifyLogin, commentController.createComment) // Cadastro de commentarios

Route
  // Rota de Controle de Sugestoes 
  .get("/suggestion/:token", verifyLogin, suggestionController.show) // lista de Sugestões
  .post("/suggestion/:token", verifyLogin, suggestionController.createSuggestion) // Cadastro de Sugestões
  .delete("/delsuggestion/:token/:id", verifyLogin, suggestionController.destroy) // Delete de Sugestões

Route
  // Rota de Controle de Warnings 
  .get("/warning/:token", verifyLogin, warningController.show) // lista de Warning
  .post("/warning/:token", verifyLogin, warningController.createwarning) // Cadastro de Warnings
  .put("/warning/:token", verifyLogin, warningController.editWarning) // Edit de Warnings
  .delete("/delwarning/:token/:id", verifyLogin, warningController.destroy) // Delete de Warnings

Route
  // Rota de Posição da Empresa 
  .get("/company/:token", verifyLogin, companyController.show) // Seleciona Posição da empresa.
  .put("/company/:token", verifyLogin, companyController.store) // Altera Localização da empresa.

Route
  // Rota de Controle de vagas 
  .get("/vacancie/:token", verifyLogin, availableController.show) // lista de Vagas
  .post("/vacancie/:token", verifyLogin, availableController.createVacancie) // Cadastro de Vagas
  .put("/vacancie/:token", verifyLogin, availableController.editVacancie) // Edição de Vagas
  .delete("/delvacancie/:token/:id", verifyLogin, availableController.destroy) // Delete de Vagas

Route
  // Rota de Ponto
  .get("/point/:token", verifyLogin, pointController.show) // lista de Ponto de todos usuarios
  .get("/lastpoint/:token", verifyLogin, pointController.showLast) // lista o ultimo ponto de todos usuarios
  .post("/userPoint/:token", verifyLogin, pointController.userPoint) // lista de Ponto do usuario
  .post("/point/:token", verifyLogin, pointController.createPoint) // Cadastro de Ponto

Route
  // Rota de report
  .get("/report/:token", verifyLogin, reportController.show) // lista de reports
  .post("/report/:token", verifyLogin, reportController.report) // Adiciona um report
  .post("/dellreport/:token", verifyLogin, reportController.destroypublics) // Deletar uma publicação e report
  .delete("/report/:token/:id", verifyLogin, reportController.destroy) // Deletar um report

Route
  // Rota de Posiçoes
  .get("/lastpositions/:token", verifyLogin, positionController.showLastPositions) // Ultimas Posições dos usuarios
  .post("/reportpositions/:token", verifyLogin, positionController.show) // lista de Ponto de Usuarios usuarios
  .post("/positions/:token", verifyLogin, positionController.store) // lista de Ponto do usuario

// rota publica bloqueada se acessada diretamente ----------------------------------
Route.get('/*', (req, res) => {
  return res.send(`<!DOCTYPE html>
  <html>
  
  <head>
      <title>WorkFlows - API</title>
      <link rel="icon" href="images/favicon.png" sizes="32x32">
      <link rel="icon" href="images/favicon.png" sizes="32x32">
	  <link rel='stylesheet' href='404/bootstrap.min.css'>
	  <link rel='stylesheet' href='404/style.css'>
	  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Arvo'>
  </head>
  
  <body>
      <section class="page_404">
          <div class="container">
              <div class="row">
                  <div class="col-sm-12 ">
                      <div class="col-sm-10 col-sm-offset-1  text-center">
                          <div class="four_zero_four_bg">
                              <h1 class="text-center ">4 0 4</h1>
                          </div>
  
                          <div class="contant_box_404">
                              <h3 class="h2">
                                  Parace que você está perdido
                              </h3>
  
                              <p>o endereço que você esta procurando não esta disponível!</p>
  
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </body>
  
  </html>`)
})
module.exports = Route;
